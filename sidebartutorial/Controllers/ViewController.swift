//
//  ViewController.swift
//  sidebartutorial
//
//  Created by Ivan Anisimov on 22/10/2018.
//  Copyright © 2018 Ivan Anisimov. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    var locationManager:CLLocationManager?;
    var mapView:GMSMapView?;
    override func viewDidLoad() {
        super.viewDidLoad()
       ;
        guard UserDefaults.standard.string(forKey: "access_token") != nil else {
           /* let loginViewController = LoginViewController()
            self.navigationController?.pushViewController(loginViewController, animated: true)*/
            dismiss(animated: true, completion: nil)
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "loginController") 
            self.present(newViewController, animated: true, completion: nil)
            return
        }
        if revealViewController() != nil{
            menuButton.target = revealViewController();
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            revealViewController().rearViewRevealWidth = 275
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        // Do any additional setup after loading the view.
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.orange]
        let camera = GMSCameraPosition.camera(withLatitude: 56.50, longitude: 60.36, zoom: 12.0)
         mapView = GMSMapView.map(withFrame: CGRect(x:0,y:60,width:UIScreen.main.bounds.width,height:(UIScreen.main.bounds.height-60)), camera: camera)
        view.addSubview(mapView!)
        
        locationManager = CLLocationManager();
        locationManager?.requestAlwaysAuthorization();
        locationManager?.requestWhenInUseAuthorization();
        if CLLocationManager.locationServicesEnabled(){
            locationManager?.delegate = self;
            locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
            locationManager?.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        mapView!.animate(to:GMSCameraPosition.camera(withLatitude: locValue.latitude, longitude: locValue.longitude, zoom: 16))
    }
   

}
