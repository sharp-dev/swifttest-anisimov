//
//  LoginViewController.swift
//  sidebartutorial
//
//  Created by Ivan Anisimov on 26/10/2018.
//  Copyright © 2018 Ivan Anisimov. All rights reserved.
//

import UIKit
import RxSwift

class LoginViewController: UIViewController {
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var errors: UILabel!
    
    private var observer: Disposable?;
    
    private var loginModel:LoginViewModel?;
    
    private let disposeBag = DisposeBag();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginModel = LoginViewModel();
    }
    
    @IBAction func login(_ sender: Any) {
        loginModel!.username = username.text!;
        loginModel!.password = password.text!;
        if observer != nil {
            observer!.dispose();
        }
        
        observer = loginModel!.login().observeOn(MainScheduler.instance).subscribe{ event in
         
            if  let token = event.element {
                UserDefaults.standard.set(token.access_token, forKey: "access_token");
                UserDefaults.standard.set(token.refresh_token, forKey: "refresh_token");
                self.GoToHomePage();
            } else if let error = event.error {
                print(event.error.debugDescription);
             
                guard let errorModel:ErrorModel = error as? ErrorModel  else {
                        self.errors.text = "something went wrong";
                        return
                    };
                
                    self.errors.text = errorModel.error_description;
                };
            
            
         };
        observer?.disposed(by: disposeBag)
    }
    
    
    func GoToHomePage(){
   
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "viewController")
            present(newViewController, animated: true, completion: nil);
       

    
    }
   

}
