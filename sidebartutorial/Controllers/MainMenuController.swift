//
//  MainMenuController.swift
//  sidebartutorial
//
//  Created by Ivan Anisimov on 10/12/2018.
//  Copyright © 2018 Ivan Anisimov. All rights reserved.
//

import Foundation
import RxSwift

class MainMenuController: UITableViewController {
   
    private let EXIT_BTN = 4;
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var exitButton: UITableViewCell!
    
    private var _httpClient:PHttpClient?;
    
    private var userModel:UserViewModel?;
    
    private let disposeBag = DisposeBag();
    
    private var exitObserver: Disposable?;
    
    private var userObserver: Disposable?;
    private var avatarObserver: Disposable?;
    
    override func viewDidLoad() {
        super.viewDidLoad();
        tableView.dataSource = self
        tableView.delegate = self
        view = tableView
        
        userModel = UserViewModel();
        
        _httpClient = CoreAssembly.instance().httpClient;
        
        userObserver = userModel?.loadCurrentUser().subscribe{
            event in
            self.username.text = self.userModel?.UserName;
            
            self.avatarObserver = self._httpClient?.loadImage(url: (self.userModel?.Photo)!).observeOn(MainScheduler.instance).subscribe{   event in
                if let image = event.element {
                    self.avatar.image = image;
                }
                
            }
           
        };
    }
 
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("row: \(indexPath.row)")
        
        if(indexPath.row == EXIT_BTN){
            
        exitObserver =  userModel?.logout().observeOn(MainScheduler.instance).subscribe{ event in
                guard event.error != nil  else {
                    if let status = event.element {
                        switch(status){
                            case 200 :
                                UserDefaults.standard.removeObject(forKey: "access_token");
                                 self.dismiss(animated: true, completion: nil);
                                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let newViewController = storyBoard.instantiateViewController(withIdentifier: "loginController") as! LoginViewController
                                self.present(newViewController, animated: true, completion: nil)
                            
                            
                        default:
                            print("unknown status")
                        }
                    }
                    
                    return;
                }
            }
        
        }
    }
    
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag, completion: completion);
        exitObserver?.dispose();
        userObserver?.dispose();
        avatarObserver?.dispose();
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
