//
//  UserViewModel.swift
//  sidebartutorial
//
//  Created by Ivan Anisimov on 19/12/2018.
//  Copyright © 2018 Ivan Anisimov. All rights reserved.
//

import Foundation
import RxSwift

class UserViewModel{
    
    private var _accountService:AccountService?;
    
    private var _httpClient:PHttpClient?;
    
    private let disposeBag = DisposeBag();
    
    
    var Id:String? ;
    var UserName:String = "";
    var Email:String = "";
    var FirstName:String = "";
    var LastName:String = "";
    var BirthDate:Date? = Date();
    var Photo:String = "";
    var Weight:Int = 0;
    var Height:Int = 0;
    var isReal:Bool = false;
    var Gender: UserModel.GenderTypes = UserModel.GenderTypes.None;
    var Avatar:UIImage?;
    
    init() {
        _accountService = ServiceAssembly.instance().accountService;
        
    }
    
    func loadCurrentUser()  -> Observable<UserModel> {
        return  (_accountService?.getCurrentUser().observeOn(MainScheduler.instance).do(onNext: {user  in
            
            self.Id =  user.Id;
            self.BirthDate = user.BirthDate;
            self.FirstName = user.FirstName ?? "";
            self.LastName = user.LastName ?? "";
            self.Height = user.Height ?? 0;
            self.Weight = user.Weight ?? 0
            self.UserName = user.UserName;
            self.Gender = user.Gender;
           
            self.Photo = user.Photo  ?? "/dist/img/avatars/img/avatars/nophoto_\(self.Gender.rawValue)_mid.jpg";
        
            self.isReal = user.IsReal
        }))!;    
        
        
        
    }
    
    func logout() -> Observable<Int> {
        return  (_accountService?.logout())!
    }
}
