//
//  LoginViewModel.swift
//  sidebartutorial
//
//  Created by Ivan Anisimov on 26/10/2018.
//  Copyright © 2018 Ivan Anisimov. All rights reserved.
//

import Foundation
import RxSwift

class LoginViewModel {
    
    var username:String;
    var password:String;
    
    private var _authService:AuthService?;
    
    
    
    init() {
        self.username = "";
        self.password = "";
        _authService = ServiceAssembly.instance().authService;
    }
    
    func login() -> Observable<TokenModel> {
        return _authService!.login(loginModel: LoginModel(viewModel: self));
    }
}
