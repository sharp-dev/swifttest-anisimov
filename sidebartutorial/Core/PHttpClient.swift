//
//  PHttpClient.swift
//  sidebartutorial
//
//  Created by Ivan Anisimov on 25/10/2018.
//  Copyright © 2018 Ivan Anisimov. All rights reserved.
//

import Foundation
import RxSwift

protocol PHttpClient {
    func get<TResponse>(url:String) -> Observable<TResponse> where TResponse:Decodable
    
    func get<TResponse>(url:String,headers:Dictionary<String,String>?  ) -> Observable<TResponse> where TResponse:Decodable
    
    func post<TRequest,TResposne>(url:String,model:TRequest) -> Observable<TResposne> where TRequest:Codable, TResposne:Decodable
    
    func post<TRequest,TResposne>(url:String,model:TRequest,headers:Dictionary<String,String>?)  -> Observable<TResposne> where TRequest:Codable, TResposne:Decodable
    
    func put<TRequest,TResposne>(url:String,model:TRequest) -> Observable<TResposne>  where TRequest:Codable, TResposne:Decodable
    
    func put<TRequest,TResposne>(url:String,model:TRequest,headers:Dictionary<String,String>?) -> Observable<TResposne>  where TRequest:Codable, TResposne:Decodable
    
    func delete<TResponse>(url:String) -> Observable<TResponse> where  TResponse:Decodable
    
    func delete<TResponse>(url:String,headers:Dictionary<String,String>?) -> Observable<TResponse> where  TResponse:Decodable
    
    func createRequest(url:String, method:String, headers: Dictionary<String, String>) -> URLRequest
    
   
    
    func downloadFile(url: String, method: String, headers:Dictionary<String,String>?) -> Observable<Data>
  
    
     func loadImage(url: String) -> Observable<UIImage> 
    
}
