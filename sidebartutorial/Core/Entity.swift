//
//  Entity.swift
//  sidebartutorial
//
//  Created by Ivan Anisimov on 25/10/2018.
//  Copyright © 2018 Ivan Anisimov. All rights reserved.
//

import Foundation

protocol Entity:Codable {
    var Id:String? { get set}
}
