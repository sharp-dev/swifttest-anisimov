//
//  HttpClient.swift
//  sidebartutorial
//
//  Created by Ivan Anisimov on 24/10/2018.
//  Copyright © 2018 Ivan Anisimov. All rights reserved.
//

import Foundation
import RxSwift

extension URLRequest {
    func makeRequest<T>() -> Observable<T> where T:Decodable  {
        return Observable<T>.create { observer in
            
            let task = URLSession.shared.dataTask(with: self) { (data, response, error) in
                guard let _ = response, let data = data else {
                    observer.on(.error(error!))
                    return
                }
                if let httpResponse = response as? HTTPURLResponse {
                    print(httpResponse.statusCode)
                    print(String(data: data, encoding: String.Encoding.utf8) ?? "{\"error\":\"incorrect_response\",\"error_description\":\"unknown server response\"}");
                    switch httpResponse.statusCode {
                        
                    case 200 :
                  
                    
                        do{
                            let result = try JSONDecoder().decode(T.self, from: data);
                            observer.on(.next(result));
                            observer.on(.completed)
                        } catch  _ {
                            do{
                                let result = try JSONDecoder().decode(ErrorModel.self, from: data);
                           
                                observer.onError(result);
                                observer.on(.completed);
                                
                            } catch let ex {
                                observer.onError(ex);
                                observer.on(.completed);
                            }
                            
                        }
                       
                    default:
                        print("no case")
                 }
             }
                
            }
            task.resume();
            
            return Disposables.create(with: task.cancel)
        }
    }
    
    func makeRequestForEmptyResponse() -> Observable<Int>  {
        return Observable<Int>.create { observer in
            
            let task = URLSession.shared.dataTask(with: self) { (data, response, error) in
                guard let _ = response, let data = data else {
                    observer.on(.error(error!))
                    return
                }
                if let httpResponse = response as? HTTPURLResponse {
                    print(httpResponse.statusCode)
               
                    
                        let string:String = String(data: data, encoding: String.Encoding.utf8) ?? "{\"error\":\"incorrect_response\",\"error_description\":\"unknown server response\"}";
                            print(string);
                
                            observer.on(.next(httpResponse.statusCode));
                            observer.on(.completed)
                       
                        
                    
                    }
                
                
            }
            task.resume();
            
            return Disposables.create(with: task.cancel)
        }
    }
    
    
}


class HttpClient:PHttpClient{
    
    public enum Status:Error {
        case unauthorize
    }
    
    
    private let disposeBag = DisposeBag();
    
    private var _baseURL:String;
    
    init(baseUrl:String){
        _baseURL = baseUrl;
    }
    public func createRequest(url:String, method:String, headers: Dictionary<String, String>) -> URLRequest{
        let url = URL(string:"\(_baseURL)\(url)");
        var request = URLRequest(url: url!);
        for (Key,Value) in headers {
            request.setValue(Value, forHTTPHeaderField:Key)
        }
        request.httpMethod = method;
        return request;
    }
    
    private func createRequest(url:String, method:String) -> URLRequest{
        return createRequest(url: url, method:method, headers: [:]);
    }
    
    public func get<TResponse>(url:String) -> Observable<TResponse> where TResponse:Decodable   {
        return get(url: url, headers: [:])
    }
    
    public func get<TResponse>(url:String,headers:Dictionary<String,String>?  ) -> Observable<TResponse> where TResponse:Decodable   {
        var request: URLRequest;
        if let rheaders = headers {
            request = createRequest(url: url, method: "GET", headers: rheaders)
        }else{
         request = createRequest(url:url, method: "GET");
        }
       return request.makeRequest();
    }
    
    public func post<TRequest,TResposne>(url:String,model:TRequest) -> Observable<TResposne> where TRequest:Codable, TResposne:Decodable {
        return post(url:url,model:model,headers: [:]);
    }
    
    public func post<TRequest,TResposne>(url:String,model:TRequest,headers:Dictionary<String,String>?)  -> Observable<TResposne> where TRequest:Codable, TResposne:Decodable  {
         var request: URLRequest;
        if let rheaders = headers {
            request = createRequest(url: url, method: "POST", headers: rheaders)
        }else{
            request = createRequest(url:url, method: "POST");
        }
        request.httpBody = try! JSONEncoder().encode(model);
        return request.makeRequest();
    }
    
    public func put<TRequest,TResposne>(url:String,model:TRequest) -> Observable<TResposne>  where TRequest:Codable, TResposne:Decodable {
        return put(url: url, model: model, headers: [:]);
    }
    
    public func put<TRequest,TResposne>(url:String,model:TRequest,headers:Dictionary<String,String>?) -> Observable<TResposne>  where TRequest:Codable, TResposne:Decodable   {
         var request: URLRequest;
        if let rheaders = headers {
            request = createRequest(url: url, method: "PUT", headers: rheaders)
        }else{
            request = createRequest(url:url, method: "PUT");
        }
        
        request.httpBody = try! JSONEncoder().encode(model);
        return request.makeRequest();
    }
    
    public func delete<TResponse>(url:String) -> Observable<TResponse> where  TResponse:Decodable {
        return delete(url: url, headers: [:]);
    }
    
    public func delete<TResponse>(url:String,headers:Dictionary<String,String>?) -> Observable<TResponse> where  TResponse:Decodable  {
        var request: URLRequest;
        if let rheaders = headers {
            request = createRequest(url: url, method: "DELETE", headers: rheaders)
        }else{
            request = createRequest(url:url, method: "DELETE");
        }
        return request.makeRequest();
    }
    
    
   
    
    
    public func downloadFile(url: String, method: String, headers:Dictionary<String,String>?) -> Observable<Data> {
        let request = createRequest(url: url, method: method, headers: (headers ?? nil)!);
        
        return Observable<Data>.create { observer in
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let _ = response, let data = data else {
                    observer.on(.error(error!))
                    return
                }
                observer.on(.next(data));
                observer.on(.completed);
                
            }
            task.resume();
            
            return Disposables.create(with: task.cancel)
        }
    }
    
    public func loadImage(url: String) -> Observable<UIImage> {
        
        return Observable<UIImage>.create{ observer in
            
         let fileObserver = self.downloadFile( url: url, method: "GET",headers:[:]).observeOn(MainScheduler.instance).subscribe{
                event in
                
                if let data = event.element {
                    let image = UIImage(data:data) ?? UIImage(named: "nophoto_0_mid", in: Bundle(for: type(of: self)), compatibleWith: nil)!;
                    observer.onNext(image )
                } else if event.error != nil {
                    observer.onError(event.error!);
                }
                observer.on(.completed);
            }
          
            return Disposables.create([fileObserver]);
            
        };
    }
    
}
