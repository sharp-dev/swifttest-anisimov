//
//  LoginModel.swift
//  sidebartutorial
//
//  Created by Ivan Anisimov on 26/10/2018.
//  Copyright © 2018 Ivan Anisimov. All rights reserved.
//

import Foundation
import RxSwift
class LoginModel:Codable{
    let grant_type = "password";
    let scope = "openid email profile offline_access roles";	
    var username:String;
    var password:String;
    
    init(viewModel:LoginViewModel){
        password = viewModel.password;
        username = viewModel.username;
    }
    
    init(username:String,password:String) {
        self.username = username;
        self.password = password;
    }
    
   
}
