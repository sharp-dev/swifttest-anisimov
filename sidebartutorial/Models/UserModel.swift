//
//  UserModel.swift
//  sidebartutorial
//
//  Created by Ivan Anisimov on 19/12/2018.
//  Copyright © 2018 Ivan Anisimov. All rights reserved.
//

import Foundation

class UserModel:Entity, Codable{
    
    public enum GenderTypes:Int{
        case None, Male, Famale, Pair
        
    };
    
    var Id: String?;
    var UserName:String;
    var Email:String?;
    var FirstName:String?;
    var LastName:String?;
    var BirthDate:Date?;
    var Photo:String?;
    var Weight:Int?;
    var Height:Int?;
    var IsReal:Bool;
    var Gender:GenderTypes
    
    private enum CodingKeys: String, CodingKey {
        case firstName
        case lastName
        case id
        case userName
        case nik
        case role
        case email
        case birthDate
        case photo
        case weight
        case height
        case isReal
        case fullName
        case jobTitle
        case phoneNumber
        case configuration
        case phone
        case gender
        case code
        case settings
        case isActive
        case password
        case registration
        case lastlogin
        case rating
        case isEnabled
        case isLockedOut
        case roles
        case balance
    }
    
    
    required init(from decoder: Decoder) throws {
         let container = try decoder.container(keyedBy: CodingKeys.self)
        Id = try container.decode(String.self, forKey: .id);
        FirstName = try? container.decode(String.self, forKey: .firstName) ;
        LastName = try? container.decode(String.self, forKey: .lastName);
        UserName = try container.decode(String.self, forKey: .userName);
        Email = try? container.decode(String.self, forKey: .email);
        BirthDate = try? container.decode(Date.self, forKey: .birthDate);
        Photo = try? container.decode(String.self, forKey: .photo);
        Weight = try? container.decode(Int.self, forKey: .weight);
        Height = try? container.decode(Int.self, forKey: .height);
        IsReal = (try? container.decode(Bool.self, forKey: .isReal)) ?? false;
        Gender =  GenderTypes(rawValue: try! container.decode(Int.self, forKey: .gender) ) ?? GenderTypes.None;

    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(FirstName, forKey: .firstName);
        try container.encode(LastName, forKey: .lastName);
        try container.encode(Id, forKey: .id);
        try container.encode(UserName, forKey: .userName);
        try container.encode(Email, forKey: .email);
        try container.encode(BirthDate, forKey: .birthDate);
        try container.encode(Weight, forKey: .weight);
        try container.encode(Height, forKey: .height);
        try container.encode(Photo, forKey: .photo);
        try container.encode(IsReal, forKey: .isReal);


        
    }
}
