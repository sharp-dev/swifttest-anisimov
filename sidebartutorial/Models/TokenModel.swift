//
//  TokenModel.swift
//  sidebartutorial
//
//  Created by Ivan Anisimov on 26/10/2018.
//  Copyright © 2018 Ivan Anisimov. All rights reserved.
//

import Foundation

class TokenModel:Codable{
    var access_token:String;
    var refresh_token:String;
    var token_type:String;
    var id_token:String;
    var expires_in: Int;
    
    
}


enum TokenError:Error {
    case doesntSet
}
