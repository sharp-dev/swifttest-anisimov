//
//  ErrorModel.swift
//  sidebartutorial
//
//  Created by Ivan Anisimov on 10/12/2018.
//  Copyright © 2018 Ivan Anisimov. All rights reserved.
//

import Foundation

class ErrorModel:LocalizedError,Codable{
    var error:String;
    var error_description:String;
}
