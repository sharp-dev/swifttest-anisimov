//
//  AccountService.swift
//  sidebartutorial
//
//  Created by Ivan Anisimov on 17/12/2018.
//  Copyright © 2018 Ivan Anisimov. All rights reserved.
//

import Foundation
import RxSwift

class AccountService : BaseService<UserModel,UserModel,UserModel> {
    
    private var _httpClient:PHttpClient;
    
    init(client:PHttpClient)  {
        _httpClient = client;
        try! super.init(url: "/api/Account", client: client);
        
    }
    
    func getCurrentUser() -> Observable<UserModel>{
        return self.get(id:"users/me");
    }
    
    func logout() -> Observable<Int> {
        return _httpClient.createRequest(url: "/api/Account/logout", method: "POST", headers: self.headers ?? [:]).makeRequestForEmptyResponse();
    }
    
    public var IsAuthenticated:Bool { get{
        return UserDefaults.standard.string(forKey: "access_token") != nil;
    }}
    
}
