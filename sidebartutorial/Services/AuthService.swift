//
//  AuthService.swift
//  sidebartutorial
//
//  Created by Ivan Anisimov on 26/10/2018.
//  Copyright © 2018 Ivan Anisimov. All rights reserved.
//

import Foundation
import RxSwift

class AuthService{
    
    var client:PHttpClient?;
    
    public func login(loginModel:LoginModel) -> Observable<TokenModel> {
       
        var request = client!.createRequest(url: "/api/connect/token", method: "POST", headers: ["Content-Type":"application/x-www-form-urlencoded"])
        request.httpBody = "scope=\(loginModel.scope)&grant_type=\(loginModel.grant_type)&username=\(loginModel.username)&password=\(loginModel.password)".data(using: String.Encoding.utf8, allowLossyConversion:true);
        
     
            return request.makeRequest();
        
    }
}
