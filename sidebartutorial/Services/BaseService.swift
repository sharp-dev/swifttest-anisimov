//
//  BaseService.swift
//  sidebartutorial
//
//  Created by Ivan Anisimov on 24/10/2018.
//  Copyright © 2018 Ivan Anisimov. All rights reserved.
//

import Foundation
import RxSwift


class BaseService<TResponse,TAllResponse,TRequest> where TRequest:Entity, TResponse:Decodable,TAllResponse:Decodable{
    
    private let _client:PHttpClient
    private let _url:String;
    public var headers:Dictionary<String,String>?
    
    init(url:String, client:PHttpClient) throws  {
        _client = client;
        _url = url;
        guard let accessToken =  UserDefaults.standard.string(forKey: "access_token") else {
            throw TokenError.doesntSet;
        }
        
        headers = ["Authorization":"Bearer \(accessToken)","Content-Type":"application/json"];
        
    }
    
    public func get(id:String) -> Observable<TResponse> {
        
        return _client.get(url:"\(_url)/\(id)", headers: headers);
    }
    
    public func get(model:TRequest) -> Observable<TResponse> {
        return _client.get(url:"\(_url)/\(String(describing: model.Id))",headers:headers);
    }
    
    public func getAll() -> Observable<TAllResponse> {
        return _client.get(url:_url,headers:headers);
    }
    
    public func post(model:TRequest) -> Observable<TResponse> {
        return _client.post(url:_url, model:model,headers:headers);
    }
    
    public func put(model:TRequest) -> Observable<TResponse> {
        return _client.put(url:"\(_url)/\(String(describing: model.Id))", model:model,headers:headers);
    }
    
    public func delete(id:String) -> Observable<TResponse> {
        return _client.delete(url:"\(_url)/\(id)",headers:headers);
    }
    
    public func delete(model:TRequest) -> Observable<TResponse> {
        return _client.delete(url:"\(_url)/\(String(describing: model.Id))",headers:headers);
    }
    
    
   
    
    
}
